package javapick.utils;

import javapick.utils.collections.ArrayStack;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ArrayStackTest {

    private ArrayStack<String> stack;

    private static final String TEST_STRING_ONE = "test";
    private final static String TEST_STRING_TWO = "test2";

    @Before
    public void init() {
        stack = new ArrayStack<>();
    }

    @Test
    public void constructorTest() {
        new ArrayStack<String>(10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorIllegalArgumentTest() {
        new ArrayStack<String>(0);
    }

    @Test
    public void pushAndPopTest() {
        stack.push(TEST_STRING_ONE);
        stack.push(TEST_STRING_TWO);
        Assert.assertEquals(TEST_STRING_TWO, stack.pop());
        Assert.assertEquals(TEST_STRING_ONE, stack.pop());
    }

    @Test(expected = IllegalArgumentException.class)
    public void pushNullTest() {
        stack.push(null);
    }

    @Test(expected = IllegalStateException.class)
    public void popFromEmptyTest() {
        stack.pop();
    }

    @Test
    public void peekTest() {
        Assert.assertNull(stack.peek());
        stack.push(TEST_STRING_ONE);
        Assert.assertEquals(TEST_STRING_ONE, stack.peek());
        Assert.assertEquals(TEST_STRING_ONE, stack.peek());
    }

    @Test
    public void isEmptyTest() {
        Assert.assertTrue(stack.isEmpty());
        stack.push(TEST_STRING_ONE);
        Assert.assertFalse(stack.isEmpty());
    }

    public void sizeTest() {

    }
}
