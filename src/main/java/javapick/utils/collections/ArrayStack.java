package javapick.utils.collections;

/**
 * Класс реализует интерфейс Stack, представляющий LIFO (last in, first out) стэк элементов.
 * Реализация основана на применении массива для хранения элементов стека. Стек имеет динамический размер.
 * При инициализации стека конструктором по умолчанию массив для хранения элементов имеет размер 10.
 * Возможно указать начальный размер массива, используя конструктор с параметром, передав в него начальный размер
 * массива. В случае заполнения массива для хранения элементов, при добавлении следующего элемента размер массива для
 * хранения элементов увеличивается вдвое.
 *
 * @param <E> тип элементов, хранимых в стеке
 */
public class ArrayStack<E> implements Stack<E> {

    /**
     * Массив для хранения элементов стека.
     */
    private Object[] elements;

    /**
     * Индекс массива, в котором находится верхний элемент стека. В пустом стеке равен -1.
     */
    private int topIndex;

    /**
     * Начальный размер массива для хранения элементов при использовании конструктора по умолчанию.
     */
    private final static int DEFAULT_INIT_CAPACITY = 10;

    /**
     * Создает пустой стек.
     */
    public ArrayStack() {
        init(DEFAULT_INIT_CAPACITY);
    }

    /**
     * Создает пустой стек с инициализацией массива для хранения элементов указанного размера.
     *
     * @param capacity начальный размер для хранения элементов
     */
    public ArrayStack(int capacity) {
        if (capacity < 1) {
            throw new IllegalArgumentException("Illegal array length: " + capacity);
        }
        init(capacity);
    }

    /**
     * Инициализация полей
     *
     * @param capacity начальный размер массива
     */
    private void init(int capacity) {
        elements = new Object[capacity];
        topIndex = -1;
    }

    /**
     * {@inheritDoc}
     */
    public void push(E element) {
        if (null == element) {
            throw new IllegalArgumentException("Element to push is null");
        }
        if (++topIndex == elements.length) {
            Object[] newStackArray = new Object[elements.length * 2];
            System.arraycopy(elements, 0, newStackArray, 0, elements.length);
            elements = newStackArray;
        }
        elements[topIndex] = element;
    }

    /**
     * {@inheritDoc}
     */
    public E pop() {
        if (isEmpty()) {
            throw new IllegalStateException("Stack is empty");
        }
        @SuppressWarnings("unchecked")
        E topElement = (E) elements[topIndex];
        elements[topIndex--] = null;
        return topElement;
    }

    /**
     * {@inheritDoc}
     *
     * @return элемент верхний элемент стека (null, если стек пустой)
     */
    @SuppressWarnings("unchecked")
    public E peek() {
        if (isEmpty()) {
            return null;
        }
        return (E) elements[topIndex];
    }

    /**
     * {@inheritDoc}
     */
    public boolean isEmpty() {
        return -1 == topIndex;
    }

    /**
     * {@inheritDoc}
     */
    public int size() {
        return topIndex + 1;
    }
}
